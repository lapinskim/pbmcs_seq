# RNA-Seq data analysis from the FABP stimulated PBMCS and macrophages

The data processing pipeline has been written in [Nexflow](https://www.nextflow.io/).

If you would like to run it locally install [Nextflow](https://www.nextflow.io/) and [conda](https://docs.conda.io/en/latest/).
First run `make` in the project main directory to download the reference genome and annotation:

    make

Provide the sample table, read FASTQ directory, reference directory, genome
FASTA file path, and annotation GTF path either in the main.nf file
or by running:

    nextflow run main.nf \
        --table “your_sample_table.csv” \
        --fastq_dir “your/fastq/directory” \
        --reference_dir “your/reference/directory” \
        --genome_fasta “reference/genome/path” \
        --annotation_gtf “reference/annotation/path”

## Prerequisites

The data analysis has been performed in [R programming language](https://www.r-project.org/).

The table below lists the required `R` packages:

| Package              | Version | Date       | Source       |
|----------------------|---------|------------|--------------|
| DESeq2               | 1.30.0  | 2020-10-27 | Bioconductor |
| BiocParallel         | 1.24.1  | 2020-11-06 | Bioconductor |
| ggplot2              | 3.3.3   | 2020-12-30 | CRAN         |
| RColorBrewer         | 1.1-2   | 2014-12-07 | CRAN         |
| pheatmap             | 1.0.12  | 2019-01-04 | CRAN         |
| apeglm               | 1.12.0  | 2020-10-27 | Bioconductor |
| magrittr             | 2.0.1   | 2020-11-17 | CRAN         |
| biomaRt              | 2.46.0  | 2020-10-27 | Bioconductor |
| data.table           | 1.13.6  | 2020-12-30 | CRAN         |
| SummarizedExperiment | 1.20.0  | 2020-10-27 | Bioconductor |
| knitr                | 1.30    | 2020-09-22 | CRAN         |
