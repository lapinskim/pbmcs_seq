#!/usr/bin/nextflow

/** Pipeline for analysis of rna-seq samples from FABP stimulated PBMCS
*/

// Parameters
params.table = "./samples.csv"
params.fastq_dir = "./reads"
params.reference_dir = "./ref"
params.out_dir = "./data"
params.genome_fasta = "${params.reference_dir}/Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa"
params.annotation_gtf = "${params.reference_dir}/Homo_sapiens.GRCh38.102.gtf"

// Program parameters
params.read_length = "151"
params.cutadapt_error = "0.1"
params.cutadapt_q = "20"
params.cutadapt_length = "10"

// Input channels
if (params.table) {
    Channel
        .fromPath(params.table, checkIfExists: true)
        .set{ table_ch }
}

Channel
    .fromPath(params.genome_fasta, checkIfExists: true)
    .into{
        genome_ch;
        genome_for_gc_ch;
        genome_for_stats_ch
    }

Channel
    .fromPath(params.annotation_gtf, checkIfExists: true)
    .into{ 
        annotation_ch;
        gtf_for_refflat_ch;
        annotation_for_counting_ch
    }

table_ch
    .splitCsv(header: true, sep:',')
    .map { row -> [
        row.name,
        [
            file(params.fastq_dir + "/" + row.read1, checkIfExists: true),
            file(params.fastq_dir + "/" + row.read2, checkIfExists: true)
        ],
        row.condition,
        row.replicate
    ] }
    .into {
        paired_fastq_for_fastqc_ch;
        paired_fastq_for_cutadapt_ch
    }

// Pipeline
process fastqc {
    conda "bioconda::fastqc=0.11.9"

    cpus "2"
    memory "20GB"

    publishDir params.out_dir, mode: "copy"

    input:
    tuple val(name), file(reads), val(condition), val(rep) from paired_fastq_for_fastqc_ch

    output:
    file "fastqc/*.{zip,html}" into fastqc_ch

    """
    mkdir -v "./fastqc"
    fastqc -o "./fastqc" -t 2 -f fastq ${reads}
    """
}

process cutadapt {
    conda "bioconda::cutadapt=2.4"

    cpus "5"
    memory "20GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(name), file(reads), val(condition), val(rep) from paired_fastq_for_cutadapt_ch

    output:
    tuple val(name), val(condition), val(rep), file("cutadapt/*_R1.trimmed.fastq.gz"), file("cutadapt/*_R2.trimmed.fastq.gz") into trimmed_fastq_for_fastqc_ch, trimmed_fastq_for_star_ch
    file "cutadapt/*.log" into cutadapt_log_ch

    """
    mkdir -v "./cutadapt"
    cutadapt \
        -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCA\
        -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT\
        --cores 10 \
        --error-rate ${params.cutadapt_error} \
        --quality-cutoff ${params.cutadapt_q} \
        --minimum-length ${params.cutadapt_length} \
        --pair-filter any \
        --output "./cutadapt/${name}_R1.trimmed.fastq.gz" \
        --paired-output "./cutadapt/${name}_R2.trimmed.fastq.gz" \
        ${reads[0]} \
        ${reads[1]} \
    | tee "cutadapt/${name}.log"
    """
}

process fastqc_trimmed {
    conda "bioconda::fastqc=0.11.8"

    cpus "2"
    memory "20GB"

    publishDir params.out_dir, mode: "copy"

    input:
    tuple val(name), val(condition), val(rep), file(read1), file(read2) from trimmed_fastq_for_fastqc_ch

    output:
    file "fastqc_trimmed/*.{zip,html}"

    """
    mkdir -v "./fastqc_trimmed"
    fastqc -o "./fastqc_trimmed" -t 2 -f fastq ${read1} ${read2}
    """
}

process star_index {
    conda "bioconda::star=2.7.7a"

    cpus "20"
    memory "62GB"

    storeDir params.reference_dir

    input:
    file genome_fasta from genome_ch
    file annotation_gtf from annotation_ch

    output:
    file "${index_dir}" into star_index_ch

    script:
    star_oh = params.read_length.toInteger() - 1
    index_name = annotation_gtf.getBaseName()
    index_dir = index_name + ".STAR.OH" + star_oh.toString() + ".index"
    """
    mkdir -v "${index_dir}"
    STAR \
        --runThreadN 20 \
        --runMode genomeGenerate \
        --genomeDir "./${index_dir}" \
        --genomeFastaFiles ${genome_fasta} \
        --sjdbGTFfile ${annotation_gtf} \
        --sjdbOverhang ${star_oh}
    """
}

process star_align {
    conda "bioconda::star=2.7.7a"
  
    cpus "32"
    memory "62GB"
  
    publishDir params.out_dir, mode: "rellink"
  
    input:
    tuple val(name), val(condition), val(rep), file(read1), file(read2) from trimmed_fastq_for_star_ch
    each file(index) from star_index_ch
  
    output:
    tuple val(name), val(condition), val(rep), file("STAR/${name}/Aligned.sortedByCoord.out.bam") into star_aligned_sorted_for_stats_ch, star_aligned_sorted_to_dedup_ch
    tuple val(name), file("STAR/${name}/Aligned.toTranscriptome.out.bam") into star_aligned_to_transcriptome_ch
    tuple val(name), file("STAR/${name}/ReadsPerGene.out.tab") into star_reads_per_gene_ch
    tuple val(name), file("STAR/${name}/Log.final.out") into star_log_final_ch
    tuple val(name), file("STAR/${name}/Unmapped.out.mate1"), file("STAR/${name}/Unmapped.out.mate2") into star_unmapped_paired_ch
    file "STAR/${name}/Log.out"
  
    """
    mkdir -pv "./STAR/${name}"
    STAR \
        --runThreadN 32 \
        --genomeDir ${index} \
        --readFilesIn ${read1} ${read2} \
        --readFilesCommand zcat \
        --outFilterType BySJout \
        --outFilterMultimapNmax 20 \
        --alignSJoverhangMin 8 \
        --alignSJDBoverhangMin 1 \
        --outFilterMismatchNmax 999 \
        --outFilterMismatchNoverReadLmax 0.04 \
        --alignIntronMin 20 \
        --alignIntronMax 1000000 \
        --alignMatesGapMax 1000000 \
        --outFileNamePrefix "./STAR/${name}/" \
        --outSAMattributes NH HI NM MD AS nM \
        --outSAMtype BAM SortedByCoordinate \
        --quantMode TranscriptomeSAM GeneCounts \
        --outSAMattrRGline ID:${name} LB:${name} SM:${name} PU:unknown PL:illumina \
        --outReadsUnmapped Fastx;
    """
}

process bamstats_gc {
    conda "./samtools.yaml"
  
    cpus "1"
    memory "10GB"
  
    input:
    file genome_fasta from genome_for_gc_ch
  
    output:
    file "${reference_name}.gc" into reference_gc_ch
  
    script:
    reference_name = genome_fasta.getName()
    """
    plot-bamstats -s ${genome_fasta} >"./${reference_name}.gc"
    """
}

process samtools_stats {
    conda "./samtools.yaml"
  
    cpus "1"
    memory "10GB"
  
    publishDir params.out_dir, mode: "rellink"
  
    input:
    tuple val(name), val(condition), val(rep), file(sorted_bam) from star_aligned_sorted_for_stats_ch
    each file(genome_fasta) from genome_for_stats_ch
  
    output:
    tuple val(name), file("STAR/${name}/${file_name}.bc") into samtools_stats_to_plot_ch, samtools_stats_for_qc_ch
  
    script:
    file_name = sorted_bam.getName()
    """
    mkdir -pv "./STAR/${name}"
    samtools stats \
        -r ${genome_fasta} \
        ${sorted_bam} \
        >"./STAR/${name}/${file_name}.bc";
    """
}

process plot_bamstats {
    conda "./samtools.yaml"
  
    cpus "1"
    memory "10GB"
  
    publishDir params.out_dir, mode: "copy"
  
    input:
    tuple val(name), file(stats_file) from samtools_stats_to_plot_ch
    each file(reference_gc) from reference_gc_ch
  
    output:
    tuple val(name), "bamstats/${name}/*" into bamstats_ch
  
    """
    plot-bamstats \
        -p "bamstats/${name}/${name}" \
        -r ${reference_gc} \
        ${stats_file}
  """
}

process mark_dup {
    conda "bioconda::picard=2.24.0"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(name), val(condition), val(rep), file(sorted_bam) from star_aligned_sorted_to_dedup_ch
    
    output:
    tuple val(name), val(condition), val(rep), file("picard/*.sorted.mkdup.bam") into sorted_mkdup_bam_ch
    file "picard/*.dup_metrics.txt" into mark_dup_ch

    """
    mkdir -v "./picard"
    picard \
        -Xmx10g \
        MarkDuplicates \
        I=${sorted_bam} \
        O=./picard/${name}.sorted.mkdup.bam \
        M=./picard/${name}.dup_metrics.txt
    """
}

process samtools_filter {
    conda "./samtools.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(name), val(condition), val(rep), file(mkdup_bam) from sorted_mkdup_bam_ch

    output:
    tuple val(name), val(condition), val(rep), file("picard/*.sorted.filtered.bam") into filtered_bam_to_index_ch, filtered_bam_for_metrics_ch, filtered_bam_for_counting_ch

    """
    mkdir -v "./picard"
    samtools view -b -q1 -f3 -F1280 ${mkdup_bam} >./picard/${name}.sorted.filtered.bam
    """
}

process samtools_index {
    conda "./samtools.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(name), val(condition), val(rep), file(filtered_bam) from filtered_bam_to_index_ch

    output:
    file "picard/*.bai"

    script:
    file_name = filtered_bam.getName()
    """
    mkdir -v "./picard"
    samtools index ${filtered_bam}
    mv -v ${file_name}.bai ./picard/
    """
}

process make_refflat {
    conda "./gtftogenepred.yaml"

    cpus "1"
    memory "10GB"

    storeDir params.reference_dir

    input:
    file(gtf) from gtf_for_refflat_ch

    output:
    file "${annotation_name}.refflat.txt" into refflat_ch

    script:
    annotation_name = gtf.getBaseName()

    shell:
    '''
    gtfToGenePred \
        -genePredExt \
        -geneNameAsName2 \
        -ignoreGroupsWithoutExons \
        !{gtf} \
        /dev/stdout | \
        awk 'BEGIN { OFS="\t"} {print $12, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}' \
        >!{annotation_name}.refflat.txt
    '''
}

process collectrnaseqmetrics {
    conda "bioconda::picard=2.24.0"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "rellink"

    input:
    tuple val(name), val(condition), val(rep), file(filtered_bam) from filtered_bam_for_metrics_ch
    each file(refflat) from refflat_ch

    output:
    file "picard/*.rna_metrics.txt" into rna_metrics_ch

    """
    mkdir -v "./picard"
    picard \
        -Xmx10g \
        CollectRnaSeqMetrics \
        I=${filtered_bam} \
        O=./picard/${name}.rna_metrics.txt \
        REF_FLAT=${refflat} \
        STRAND=SECOND_READ_TRANSCRIPTION_STRAND
    """
}

process htseq_count {
    conda "./htseq.yaml"

    cpus "1"
    memory "20GB"

    publishDir params.out_dir, mode: "copy"

    input:
    tuple val(name), val(condition), val(rep), file(filtered_bam) from filtered_bam_for_counting_ch
    each file(annotation_gtf) from annotation_for_counting_ch

    output:
    file "./htseq_count/${name}.counts.tsv" into counts_ch

    """
    mkdir -v "./htseq_count"
    htseq-count \
        --format bam \
        --order pos \
        --stranded reverse \
        -a 10 \
        --type exon \
        --idattr gene_id \
        --mode union \
        ${filtered_bam} \
        ${annotation_gtf} \
        >"./htseq_count/${name}.counts.tsv"
    """
}

process rename_for_multiqc {
    cpus "1"
    memory "1GB"

    input:
    tuple val(name), file(star_file) from star_reads_per_gene_ch.mix(star_log_final_ch, samtools_stats_for_qc_ch)

    output:
    file "${name}.${star_file.getName()}" into star_stats_ch

    """
    cp -v ${star_file} ${name}.${star_file.getName()}
    """
}

process change_name_for_multiqc {
    cpus "1"
    memory "1GB"

    input:
    file(metrics) from mark_dup_ch

    output:
    file "*.renamed.dup_metrics.txt" into renamed_dup_metrics_ch

    """
    sed 's/INPUT=\\[.*\\]/INPUT=[${metrics.getSimpleName()}]/g' ${metrics} >${metrics.getSimpleName()}.renamed.dup_metrics.txt
    """
}

process multiqc {
    conda "./multiqc.yaml"

    cpus "1"
    memory "10GB"

    publishDir params.out_dir, mode: "copy"

    input:
    file "cutadapt/*" from cutadapt_log_ch.collect()
    file "fastqc/*" from fastqc_ch.collect()
    file "STAR/*" from star_stats_ch.collect()
    file "htseq_count/*" from counts_ch.collect()
    file "picard/*" from rna_metrics_ch.collect()
    file "picard/*" from renamed_dup_metrics_ch.collect()

    output:
    file "multiqc/multiqc_report.html"
    file "multiqc/*"

    """
    mkdir -v "./multiqc"
    multiqc -o "./multiqc" .
    """
}
