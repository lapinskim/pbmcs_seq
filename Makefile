# Makefile for the rna-seq of samples from FABP stimulated PBMCS

REF_DIR = ./ref
SOURCE_URL = ftp://ftp.ensembl.org/pub
RELEASE = release-102
ANNOTATION = Homo_sapiens.GRCh38.102.gtf
SEQUENCE = Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa
REFFLAT = refFlat.txt


all : $(REF_DIR)/$(ANNOTATION) $(REF_DIR)/$(SEQUENCE)
.PHONY : all

$(REF_DIR)/$(ANNOTATION) : | $(REF_DIR)
	wget -P $(REF_DIR) $(SOURCE_URL)/$(RELEASE)/gtf/homo_sapiens/$(ANNOTATION).gz
	gunzip $@.gz

$(REF_DIR)/$(SEQUENCE) : | $(REF_DIR)
	wget -P $(REF_DIR) $(SOURCE_URL)/$(RELEASE)/fasta/homo_sapiens/dna/$(SEQUENCE).gz
	gunzip $@.gz

$(REF_DIR) :
	mkdir -v -p $@
